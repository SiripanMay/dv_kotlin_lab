package dvkotlin.demo.config

import dvkotlin.demo.entity.*
import dvkotlin.demo.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader:ApplicationRunner{
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    @Autowired
    lateinit var productRepository: ProductRepository
    @Autowired
    lateinit var addressRepository: AddressRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository
    @Autowired
    lateinit var dataLoader: DataLoader

    @Transactional
    override fun run(args: ApplicationArguments?) {
        val manu1 = manufacturerRepository.save(Manufacturer("CAMT","0000000"))
        val manu2 = manufacturerRepository.save(Manufacturer("Apple","053123456"))
        val manu3 = manufacturerRepository.save(Manufacturer("Samsung","555666777888"))

        val address1 = addressRepository.save(Address("ถนนอนุสาวรีย์ประชาธิปไตย","แขวง ดินสอ","เขตดุสิต","กรุงเทพ ","10123"))
        val customer1 = customerRepository.save(Customer("Lung","pm@go.th",UserStatus.ACTIVE))

        val address2 = addressRepository.save(Address("239","มหาวิทยาลัยเชียงใหม่","ต.สุเทพ อ.เมือง","จ.เชียงใหม่","50200"))
        val customer2 = customerRepository.save(Customer("ชัชชาติ","chut@taopoon.com",UserStatus.ACTIVE))

        val address3 = addressRepository.save(Address("ซักที่บนโลก","ต.สุขสันต์","อ.ในเมือง","จ.ขอนแก่","12457"))
        val customer3 = customerRepository.save(Customer("ธนาธร","thanathorn@life.com",UserStatus.PENDING))

        customer1.defaultAddress = address1
        customer2.defaultAddress = address2
        customer3.defaultAddress = address3

        val product1 = productRepository.save(Product("CAMT","The best College in CMU",0.00,1,"http://www.camt.cmu.ac.th/th/images/logo.jpg"))
        val product2 = productRepository.save(Product("iPhone","It's a phone",28000.00,20,"https://www.jaymartstore.com/Products/iPhone-X-64GB-Space-Grey--1140900010552--4724"))
        val product3 = productRepository.save(Product("Note 9","Other Iphone",28001.00,10,"http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"))
        val product4 = productRepository.save(Product("Prayuth","The best PM ever",1.00,1,"https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"))
        manu1.product.add(product1)
        product1.manufacturer=manu1
        manu1.product.add(product4)
        product4.manufacturer=manu1
        manu2.product.add(product2)
        product2.manufacturer=manu2
        manu3.product.add(product3)
        product3.manufacturer=manu3

        val selectedProduct1 = selectedProductRepository.save(SelectedProduct(4))
        selectedProduct1.product=product2
        val selectedProduct2 = selectedProductRepository.save(SelectedProduct(1))
        selectedProduct2.product=product4
        val shoppingCart1=shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.SENT))
        shoppingCart1.selectedProduct.addAll(listOf(selectedProduct1,selectedProduct2))
        shoppingCart1.customer = customer1

        val selectedProduct3 = selectedProductRepository.save(SelectedProduct(1))
        selectedProduct3.product=product4
        val selectedProduct4 = selectedProductRepository.save(SelectedProduct(1))
        selectedProduct4.product=product1
        val selectedProduct5 = selectedProductRepository.save(SelectedProduct(2))
        selectedProduct5.product=product3
        val shoppingCart2=shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.WAIT))
        shoppingCart2.selectedProduct.addAll(listOf(selectedProduct3,selectedProduct4,selectedProduct5))
        shoppingCart2.customer = customer2

        dataLoader.loadData()



    }
}