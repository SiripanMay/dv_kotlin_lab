package dvkotlin.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

//data class Product(var name:String,var description :String,var price :Int,var quantity :Int)
@Entity
data class Product (var name: String? = null,
                    var description: String? = null,
                    var price: Double? = null,
                    var amountInStock:Int? = null,
                    var imageUrl:String?=null,
                    var  isDeleted:Boolean=false){
    @Id
    @GeneratedValue
    var id:Long? = null
    @ManyToOne
    var manufacturer: Manufacturer? = null
    constructor(name: String,
                description: String,
                price: Double,
                amountInStock: Int,
                manufacturer: Manufacturer,
                imageUrl: String?):
            this(name, description, price, amountInStock, imageUrl){
        this.manufacturer=manufacturer
    }
}