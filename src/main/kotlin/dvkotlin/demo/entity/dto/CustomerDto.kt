package dvkotlin.demo.entity.dto


import dvkotlin.demo.entity.User
import dvkotlin.demo.entity.UserStatus

data class CustomerDto (
        override var name: String?= null,
        override var email: String?=null,
        override var userStatus: UserStatus? = UserStatus.PENDING,
        var defaultAddress : AddressDto?=null,
        var id:Long?=null
):User