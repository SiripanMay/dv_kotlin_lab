package dvkotlin.demo.entity

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

data class Person(var name:String, var surname:String,var age:Int)

data class Persons(var name:String, var surname:String,var team:String, var number:Int)