package dvkotlin.demo.entity

import javax.persistence.*

@Entity
data class Customer(
    override var name: String?=null,
    override var email: String?=null,
    override var userStatus: UserStatus? = UserStatus.PENDING,
    var  isDeleted:Boolean=false
): User{
    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToMany
    var shippingAddress = mutableListOf<Address>()
    @OneToOne
    var billingAddress: Address?=null
    @OneToOne
    var defaultAddress: Address?=null
}