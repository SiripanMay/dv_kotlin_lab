package dvkotlin.demo.dao

import dvkotlin.demo.entity.Customer
import dvkotlin.demo.entity.UserStatus
import dvkotlin.demo.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDaoDBImpl:CustomerDao{
    override fun findById(id: Long): Customer? {
        return customerRepository.findById(id).orElse(null)
    }

    override fun saveWithNew(customer: Customer): Customer {
        return customerRepository.save(customer)
    }

    override fun save(customer: Customer): Customer {
        return customerRepository.save(customer)
    }

    override fun getCustomerByStatus(status: UserStatus): List<Customer> {
        return customerRepository.findByUserStatus(status)
    }


    override fun getCustomerByAddress(province: String): List<Customer> {
        return customerRepository.findByDefaultAddress_ProvinceContainingIgnoreCase(province)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name, email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerRepository.findByNameEndingWith(name)
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerRepository.findByName(name)
    }

    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getCustomers(): List<Customer> {
        //return customerRepository.findAll().filterIsInstance(Customer::class.java)
        return customerRepository.findByIsDeletedIsFalse()
    }
}
