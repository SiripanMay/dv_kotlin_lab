package dvkotlin.demo.dao

import dvkotlin.demo.entity.ShoppingCart
import dvkotlin.demo.repository.ShoppingCartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShoppingCartDBImpl:ShoppingCartDao{
    override fun getShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingRepository.findAll(PageRequest.of(page,pageSize))
    }

    override fun getCustomerByProduct(name: String): List<ShoppingCart> {
        return shoppingRepository.findBySelectedProduct_Product_NameContainingIgnoreCase(name)
    }

    override fun getShoppingCartByProductNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingRepository.findBySelectedProduct_Product_NameContainingIgnoreCase(name, PageRequest.of(page,pageSize))
    }

    override fun getShoppingCartByProductName(name: String): List<ShoppingCart> {
        return shoppingRepository.findBySelectedProduct_Product_NameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var shoppingRepository: ShoppingCartRepository

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingRepository.findAll().filterIsInstance(ShoppingCart::class.java)
    }
}
