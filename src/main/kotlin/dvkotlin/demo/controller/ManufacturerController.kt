package dvkotlin.demo.controller

import dvkotlin.demo.entity.dto.ManufacturerDto
import dvkotlin.demo.servive.ManufacturerService
import dvkotlin.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ManufacturerController{
    @Autowired
    lateinit var manufacturerService: ManufacturerService
    @GetMapping ("/manufacturer")
    fun getAllManufacturer(): ResponseEntity<Any> {
        return ResponseEntity.ok(manufacturerService.getManufacturers())
    }

    @PostMapping("/manufacturer")
    fun addManufacuturer(@RequestBody manu:ManufacturerDto):ResponseEntity<Any>{
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapManufacturer(manufacturerService.save(manu)))
    }

    @PutMapping("/manufacturer/{manuId}")
    fun updateManufacturer(@PathVariable("manuId") id:Long?,
                           @RequestBody manu: ManufacturerDto):ResponseEntity<Any>{
        manu.id = id
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapManufacturer(manufacturerService.save(manu)))
    }
}