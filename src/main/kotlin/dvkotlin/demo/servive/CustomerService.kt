package dvkotlin.demo.servive

import dvkotlin.demo.entity.Customer
import dvkotlin.demo.entity.UserStatus

interface CustomerService{
    fun getCustomers():List<Customer>
    fun getCustomerByName(name:String):Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByAddress(province: String): List<Customer>
    fun getCustomerByStatus(status: UserStatus): List<Customer>
    fun save(addressId:Long,customer: Customer): Customer
    fun saveWithNew(customer: Customer): Customer
    fun remove(id: Long): Customer?


}