package dvkotlin.demo.servive

import dvkotlin.demo.dao.AddressDao
import dvkotlin.demo.dao.CustomerDao
import dvkotlin.demo.entity.Customer
import dvkotlin.demo.entity.UserStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional


@Service
class CustomerServiceImpl:CustomerService{
    @Transactional
    override fun remove(id: Long): Customer? {
        val customer = customerDao.findById(id)
        customer?.isDeleted = true
        return customer
    }

    @Transactional
    override fun saveWithNew(customer: Customer): Customer {
        val defaultAddress = customer.defaultAddress?.let{addressDao.save(it)}
        val customer =customerDao.save(customer)
        customer.defaultAddress = defaultAddress
        return customer
    }

    @Transactional
    override fun save(addressId:Long,customer: Customer): Customer {
        val defaultAddress = addressDao.findById(addressId)
        val customer =customerDao.save(customer)
        customer.defaultAddress = defaultAddress
        return customer
    }


    override fun getCustomerByStatus(status: UserStatus): List<Customer> {
        return customerDao.getCustomerByStatus(status)
    }


    override fun getCustomerByAddress(province: String): List<Customer> {
        return customerDao.getCustomerByAddress(province)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerDao.getCustomerByPartialNameAndEmail(name, email)
    }


    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerDao.getCustomerByPartialName(name)
    }

    override fun getCustomerByName(name: String): Customer?
            = customerDao.getCustomerByName(name)

    @Autowired
    lateinit var customerDao: CustomerDao
    @Autowired
    lateinit var addressDao: AddressDao

    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }
}
